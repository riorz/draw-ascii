import pathlib
import argparse
import tempfile

from PIL import Image, ImageDraw, ImageFont
from pyfiglet import Figlet

# Default color
COLOR, BGCOLOR = 'white', 'black'
FONT = 'standard'
IMG_FMT = 'png'

def _get_file_name(ext):
    try:
        tf = tempfile.NamedTemporaryFile(dir='./tmp')
    except FileNotFoundError:
        pathlib.Path('./tmp').mkdir()
        tf = tempfile.NamedTemporaryFile(dir='./tmp')
    return f'{tf.name}.{ext}'

def draw(words):
    filename = _get_file_name(IMG_FMT)
    fnt = ImageFont.truetype('/usr/share/fonts/truetype/ubuntu/UbuntuMono-R.ttf', 29)

    # First create a figlet.
    text = Figlet(font=FONT).renderText(words)
    # Draw a fake image to calculate text size.
    img = Image.new('RGBA', (1,1))
    draw = ImageDraw.Draw(img)
    textsize = draw.textsize(text, fnt)

    # use text size to create real image.
    imgsize = (textsize[0]+40, textsize[1])
    img = Image.new('RGBA', imgsize, BGCOLOR)
    draw = ImageDraw.Draw(img)
    draw.text((20, 0), text, font=fnt, fill=COLOR)
    img.save(filename, IMG_FMT)
    return filename


def main():
    global COLOR, BGCOLOR, FONT
    parser = argparse.ArgumentParser(description='I will draw you a picture with your words!')
    parser.add_argument('words', type=str, help='Give me some words.')
    parser.add_argument('--c', dest='color', type=str, help='Font color.')
    parser.add_argument('--bg', dest='bgcolor', type=str, help='Background color.')
    parser.add_argument('--f', dest='font', type=str, help='Figlet font.')

    args = parser.parse_args()
    if args.color:
        COLOR = args.color
    if args.bgcolor:
        BGCOLOR = args.bgcolor
    if args.font:
        FONT = args.font

    image = draw(args.words)
    print(f'Successfully created image: {image}')


if __name__ == '__main__':
    main()
